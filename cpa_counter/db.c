
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <mysql/mysql.h>



#include "config.h"
#include "log.h"
#include "db.h"




#define CPA_COUNTER_INSERT_OR_UPDATE_STMT "INSERT INTO `cpa_counter`(`CPA_SESSION_KEY`, `CPA_UNIQUE_MARK`, `JS_LOCATION`, `JS_REFERER`, `REMOTE_ADDR`, `REMOTE_PORT`, `HTTP_ENV`, `CPA_START_TIME`, `CPA_END_TIME`) VALUES(?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `CPA_END_TIME`=VALUES(`CPA_END_TIME`)"









typedef struct {
    char               *buf;
    unsigned long       len;
} my_str_t;

static void my_str_bind(MYSQL_BIND *bind, my_str_t *str);
static my_str_t* my_get_var(char *query_string, char *key);
static int my_hex_to_binary(char *hex, char *binary, size_t binary_len);




static MYSQL           *mysql;
static MYSQL_STMT      *cpa_counter_stmt;



int cpa_db_init() {


    if(!(mysql = mysql_init(NULL))) {
        cpa_log_err("Failed to initate MySQL connection\n");
        goto error;
    }

    my_bool reconnect = 1;
    mysql_options(mysql, MYSQL_OPT_RECONNECT, &reconnect);


    if (!mysql_real_connect(mysql, DBHOST, DBUSER, DBPASSWD, DBDB, DBPORT, DBUNIXSOCKET, 0)) {
        cpa_log_err("Failed to connect to MySQL: Error: %s\n", mysql_error(mysql));
        goto error;
    }

    /* Note: for each new thread */
    mysql_thread_init();



    mysql_autocommit(mysql, 1);

    if (mysql_query(mysql, "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")) {
        cpa_log_err("Failed to set transaction isolation level: Error: %s\n", mysql_error(mysql));
        goto error;
    }
    
    
    
    
    
    
    if (!(cpa_counter_stmt = mysql_stmt_init(mysql))) {
        cpa_log_err("mysql_stmt_init(), out of memory\n");
        goto error;
    }
    if (mysql_stmt_prepare(cpa_counter_stmt, CPA_COUNTER_INSERT_OR_UPDATE_STMT, strlen(CPA_COUNTER_INSERT_OR_UPDATE_STMT))) {
        cpa_log_err("%s\n", mysql_stmt_error(cpa_counter_stmt));
        goto error;
    }
    
    
    
    
    
    
    
    return 0;
    
error:
    return 1;
}



int cpa_db_del() {
    mysql_thread_end();
    
    if (cpa_counter_stmt)
        mysql_stmt_close(cpa_counter_stmt);

    if (mysql)
        mysql_close(mysql);
    
    
    return 0;
}





int cpa_db_update(FCGX_ParamArray envp, char *cpa_cookie) {
    
    MYSQL_BIND      bind[9];
    MYSQL_BIND     *bindp;
    unsigned long mysql_tid;
    
    my_str_t       *cpa_unique_mark;
    my_str_t       *js_location;
    my_str_t       *js_referer;
    my_str_t       *http_env;
    
    cpa_unique_mark = js_location = js_referer = http_env = NULL;
    bindp = bind;

    
    mysql_tid = mysql_thread_id(mysql);
    if (mysql_ping(mysql)) {
        cpa_log_err("MySQL ping error: Error: %s\n", mysql_error(mysql));
        goto error;
    }
    if (mysql_tid != mysql_thread_id(mysql)) {

        /* A reconnect occured, stmt was released */
        if (!(cpa_counter_stmt = mysql_stmt_init(mysql))) {
            cpa_log_err("mysql_stmt_init(), out of memory\n");
            goto error;
        }
        if (mysql_stmt_prepare(cpa_counter_stmt, CPA_COUNTER_INSERT_OR_UPDATE_STMT, strlen(CPA_COUNTER_INSERT_OR_UPDATE_STMT))) {
            cpa_log_err("%s\n", mysql_stmt_error(cpa_counter_stmt));
            goto error;
        }
    }
    
    
    
    
    
    
    
    
    
    memset(bind, 0, sizeof(bind));

    char        cpa_session_key_bin[CPA_COOKIE_LEN/2];
    {
        if (my_hex_to_binary(cpa_cookie, cpa_session_key_bin, sizeof(cpa_session_key_bin)))
            goto error;
        
        bindp->buffer_type     = MYSQL_TYPE_BLOB;
        bindp->buffer          = cpa_session_key_bin;
        bindp->buffer_length   = sizeof(cpa_session_key_bin);
        bindp++;
    }

    char        cpa_unique_mark_bin[20];
    {
        char   *query_string;
        
        query_string = FCGX_GetParam("QUERY_STRING", envp);
        if (!query_string) {
            cpa_log_err("no QUERY_STRING env var");
            goto error;
        }

        if (!(cpa_unique_mark = my_get_var(query_string, "cpa_unique_mark=")))
            goto error;
        if (!(js_location = my_get_var(query_string, "location=")))
            goto error;
        if (!(js_referer = my_get_var(query_string, "referrer=")))
            goto error;

        if (my_hex_to_binary(cpa_unique_mark->buf, cpa_unique_mark_bin, sizeof(cpa_unique_mark_bin)))
            goto error;
        bindp->buffer_type     = MYSQL_TYPE_BLOB;
        bindp->buffer          = cpa_unique_mark_bin;
        bindp->buffer_length   = sizeof(cpa_unique_mark_bin);
        bindp++;

        
        my_str_bind(bindp++, js_location);
        my_str_bind(bindp++, js_referer);
    }
    
    {
        my_str_t        remote_addr;
        
        remote_addr.buf = FCGX_GetParam("REMOTE_ADDR", envp);
        if (!remote_addr.buf) {
            cpa_log_err("no REMOTE_ADDR env var");
            goto error;
        }
        remote_addr.len = strlen(remote_addr.buf);
        my_str_bind(bindp++, &remote_addr);
    }
    
    {
        my_str_t        remote_port;
        
        remote_port.buf = FCGX_GetParam("REMOTE_PORT", envp);
        if (!remote_port.buf) {
            cpa_log_err("no REMOTE_PORT env var");
            goto error;
        }
        remote_port.len = strlen(remote_port.buf);
        my_str_bind(bindp++, &remote_port);
    }
    
    
    {
        char          **e;
        char           *p;
        
        
        http_env = malloc(sizeof(my_str_t));
        if (!http_env) {
            cpa_log_syserr("malloc");
            goto error;
        }
        http_env->buf = NULL;
        
        
        http_env->len = 0;
        for(e = envp ; *e != NULL; e++) {
            size_t len;
            
            len = strlen(*e);
            if (len > 5 && !memcmp("HTTP_", *e, 5)) {
                http_env->len += len + 1;
            }
        }
        
        /* assume HTTP_* env variables MUST exist */
        http_env->buf = malloc(http_env->len);
        if (!http_env->buf) {
            cpa_log_syserr("malloc");
            goto error;
        }
        p = http_env->buf;
        for(e = envp ; *e != NULL; e++) {
            size_t len;
            
            len = strlen(*e);
            if (len > 5 && !memcmp("HTTP_", *e, 5)) {
                strcpy(p, *e);
                p += strlen(*e);
                *p++ = '\n';
            }
        }
        http_env->len--;        /* remove trailing \n */
        
        
        my_str_bind(bindp++, http_env);
    }
    
    {
        MYSQL_TIME      t;
        
        struct tm   tmp;
        time_t      tmp_time;
        
        
        if ((tmp_time = time(NULL)) == ((time_t) -1)) {
            cpa_log_syserr("time");
            goto error;
        }
        
        if (!localtime_r(&tmp_time, &tmp)) {
            cpa_log_syserr("localtime_r");
            goto error;
        }
        
        memset(&t, 0, sizeof(t));
        t.year   = tmp.tm_year + 1900;
        t.month  = tmp.tm_mon + 1;
        t.day    = tmp.tm_mday;
        t.hour   = tmp.tm_hour;
        t.minute = tmp.tm_min;
        t.second = tmp.tm_sec;
        
        bindp->buffer_type     = MYSQL_TYPE_TIMESTAMP;
        bindp->buffer          = &t;
        bindp++;

        bindp->buffer_type     = MYSQL_TYPE_TIMESTAMP;
        bindp->buffer          = &t;
        bindp++;
    }
    


    if (mysql_stmt_bind_param(cpa_counter_stmt, bind)) {
        cpa_log_err("%s\n", mysql_stmt_error(cpa_counter_stmt));
        goto error;
    }

    if (mysql_stmt_execute(cpa_counter_stmt)) {
        cpa_log_err("%s\n", mysql_stmt_error(cpa_counter_stmt));
        goto error;
    }
    
    
    
    
    
    
    if (cpa_unique_mark) {
        free(cpa_unique_mark->buf);
        free(cpa_unique_mark);
    }
    if (js_location) {
        free(js_location->buf);
        free(js_location);
    }
    if (js_referer) {
        free(js_referer->buf);
        free(js_referer);
    }
    if (http_env) {
        free(http_env->buf);
        free(http_env);
    }
    
    return 0;
    
error:

    if (cpa_unique_mark) {
        free(cpa_unique_mark->buf);
        free(cpa_unique_mark);
    }
    if (js_location) {
        free(js_location->buf);
        free(js_location);
    }
    if (js_referer) {
        free(js_referer->buf);
        free(js_referer);
    }
    if (http_env) {
        free(http_env->buf);
        free(http_env);
    }

    return 1;
}




static void my_str_bind(MYSQL_BIND *bind, my_str_t *str) {
    bind->buffer_type     = MYSQL_TYPE_STRING;
    bind->buffer          = str->buf;
    bind->buffer_length   = str->len;
    bind->is_null         = 0;
    bind->length          = &str->len;
}


static my_str_t* my_get_var(char *query_string, char *key) {
    
    my_str_t   *res;
    my_str_t    tmp;
    char       *p;
    char       *b;
    
    res = malloc(sizeof(my_str_t));
    if (!res) {
        cpa_log_syserr("malloc");
        goto error;
    }
    
    tmp.buf = strstr(query_string, key);
    if (!tmp.buf)
        goto error;
    
    tmp.buf += strlen(key);
    for (p = tmp.buf; ; p++) {
        if (*p == '&' || *p == '\0')
            break;
    }
    tmp.len = p - tmp.buf;
    res->buf = malloc(tmp.len);
    if (!res->buf)
        goto error;
    
    for (p = tmp.buf, b = res->buf; p < tmp.buf + tmp.len; p++, b++) {
        if (*p == '%' && p + 2 < tmp.buf + tmp.len) {
            char tmp_hex[] = "0xXX";
            tmp_hex[2] = *++p;
            tmp_hex[3] = *++p;
            
            char   *endptr = NULL;
            *b = strtol(tmp_hex, &endptr, 16);
            if (*endptr) {
                cpa_log_err("cannot convert to int %s", tmp_hex);
                goto error;
            }
        }
        else
            *b = *p;
    }
    res->len = b - res->buf;
    
    
    return res;
    
error:
    if (res) {
        free(res->buf);
        free(res);
    }
    
    return NULL;
}

int my_hex_to_binary(char *hex, char *binary, size_t binary_len) {
    char       *p, *b;
    
    for (p = hex, b = binary; b < binary + binary_len; b++) {
        char tmp_hex[] = "0xXX";
        tmp_hex[2] = *p++;
        tmp_hex[3] = *p++;
        
        char   *endptr = NULL;
        *b = strtol(tmp_hex, &endptr, 16);
        if (*endptr) {
            cpa_log_err("cannot convert to int %s", tmp_hex);
            return 1;
        }
    }
    
    
    return 0;
}




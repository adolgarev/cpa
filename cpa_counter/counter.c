
#include <fcgi_config.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif


#include <fcgiapp.h>




#include "config.h"
#include "db.h"
#include "log.h"





/* Returns cpa_cookie that identifies user or NULL in case of error,
 sets this cookie in case it was not set,
 returned memory is statically allocated, you do not need to free it,
 but it is not thread-safe */
static char tmp_cpa_cookie[CPA_COOKIE_LEN + 1];
static char* cpa_set_cookie(FCGX_Stream *in, FCGX_Stream *out, FCGX_Stream *err, FCGX_ParamArray envp) {
    char       *cpa_cookie;
    char       *cookies;
    
    cookies = FCGX_GetParam("HTTP_COOKIE", envp);
    if (cookies)
        cpa_cookie = strstr(cookies, "cpa_cookie=");
    
    if (!cookies || !cpa_cookie) {
        // set new cpa_cookie
        char   *p;
        for (p = tmp_cpa_cookie; p < tmp_cpa_cookie + CPA_COOKIE_LEN; p++) {
            int r;
            r = rand() % 16;
            if (r < 10)
                r += 48;
            else
                r += 87;
            *p = r;
        }
        tmp_cpa_cookie[CPA_COOKIE_LEN] = '\0';
        
        
        
        char    expires_buf[100];
        {
            struct tm   tmp_tm;
            time_t      tmp_time;
            
            if ((tmp_time = time(NULL)) == ((time_t) -1)) {
                cpa_log_syserr("time");
                goto error;
            }
            
            if (!localtime_r(&tmp_time, &tmp_tm)) {
                cpa_log_syserr("localtime_r");
                goto error;
            }
            
            tmp_tm.tm_year++;

            if (!strftime(expires_buf, sizeof(expires_buf), "%a, %d %b %Y %T %Z", &tmp_tm)) {
                cpa_log_syserr("strftime");
                goto error;
            }
        }
        
        
        /* IE 6,7,8 doesn't support Max-Age, use Expires :( */
        FCGX_FPrintF(out, "Set-Cookie: cpa_cookie=%s; Expires=%s\r\n", tmp_cpa_cookie, expires_buf);
    }
    else {
        memcpy(tmp_cpa_cookie, cpa_cookie + sizeof("cpa_cookie=") - 1, CPA_COOKIE_LEN);
        tmp_cpa_cookie[CPA_COOKIE_LEN] = '\0';
    }
    
    return tmp_cpa_cookie;
    
error:
    return NULL;
}







int main () {
    FCGX_Stream *in, *out, *err;
    FCGX_ParamArray envp;
    
    {
        srand(time(NULL));
    }
    
    
    if (cpa_db_init())
        goto error;
    
    
    {
        int sock;
        
        sock = FCGX_OpenSocket(SOCKET, BACKLOG);
        if (sock == -1) {
            goto error;
        }
        
        if (dup2(sock, 0) == -1) {
            perror("dup2");
            goto error;
        }
        
        close(sock);
    }
    

    while (FCGX_Accept(&in, &out, &err, &envp) >= 0) {
        char   *cpa_cookie;

        FCGX_FPrintF(out, "Content-Type: application/x-javascript\r\n");

        if (!(cpa_cookie = cpa_set_cookie(in, out, err, envp)))
            continue;
        cpa_db_update(envp, cpa_cookie);
        
        FCGX_FPrintF(out, "\r\nCPASendRequestDelayed();\n");
    }
    
    cpa_db_del();

    return 0;
    
error:
    return 1;
}

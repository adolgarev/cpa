
#ifndef __CONFIG_H__
#define __CONFIG_H__

#define SOCKET                  "127.0.0.1:9000"
#define BACKLOG                 10000


#define CPA_COOKIE_LEN          32


#define DBHOST                  "localhost"
#define DBUSER                  "root"
#define DBPASSWD                NULL
#define DBDB                    "cpa"
#define DBPORT                  0
#define DBUNIXSOCKET            "/var/run/mysqld/mysqld.sock"
#define CPA_COUNTER_TABLE       "cpa_counter"




#endif

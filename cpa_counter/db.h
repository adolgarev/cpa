
#ifndef __DB_H__
#define __DB_H__


#include <fcgiapp.h>


int cpa_db_init();
int cpa_db_del();
int cpa_db_update(FCGX_ParamArray envp, char *cpa_cookie);

#endif

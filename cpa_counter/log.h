/* Logging routines */

#ifndef __LOG_H__
#define __LOG_H__

#define cpa_log_err(fmt, ...)       \
    fprintf(stderr, "%s:%d ERROR: " fmt "\n", \
    __FILE__, __LINE__, ##__VA_ARGS__)

#define cpa_log_syserr(fmt, ...)       \
    fprintf(stderr, "%s:%d ERROR: " fmt "[%m]\n", \
    __FILE__, __LINE__, ##__VA_ARGS__)


#endif

